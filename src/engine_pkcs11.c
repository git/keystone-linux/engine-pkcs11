/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * Copyright (c) 2002 Juha Yrjölä.  All rights reserved.
 * Copyright (c) 2001 Markus Friedl.
 * Copyright (c) 2002 Olaf Kirch
 * Copyright (c) 2003 Kevin Stefanik
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
 ChangeLog (01/04/2013):
  Added following new functionalities
   - API to list objects on the token
   - API to store certificate to token
   - API to generate & store RSA key pair to token
   - API to get RSA public key from token
   - API to Delete object from token
 *****************************************************************************/

#include <config.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/objects.h>
#include <openssl/engine.h>
#include <libp11.h>
#include "engine_pkcs11.h"
#include <syslog.h>

#ifdef _WIN32
#define strncasecmp strnicmp
#endif

#define fail(msg) { Log(LOG_ERR,msg); return NULL;}

/** The maximum length of an internally-allocated PIN */
#define MAX_PIN_LENGTH   32
#define FLAGS_LEN 64
/* Maximum size of a token object */
#define MAX_OBJECT_SIZE	5000

struct cert_info {
	unsigned char *id;
	size_t id_len;
	char *label;
	X509 *x509;
};

struct enum_certs_ctrl_param {
	int slot_nr;
	int cnt;
	struct cert_info *info;
};

struct cert_ctrl_params {
	int slot_nr;
	unsigned char *cert_id;
	size_t cert_id_len;
	char *cert_label;
	X509 *cert;
};

static int do_login(PKCS11_SLOT *slot);
static int pkcs11_store_cert
(
	ENGINE * e,
	int slot_nr,
	unsigned char *cert_id,
	size_t cert_id_len,
	char *cert_label,
	X509 *x
);
static int parse_cert_store_string
(
	const char *cert_store_str,
	int *slot,
	unsigned char *id,
	size_t * id_len,
	char **label,
	char **cert_file
);
static int parse_key_gen_string
(
	const char *key_gen_str,
	int *slot,
	int *key_size,
	unsigned char *id,
	size_t * id_len,
	char **label
);
static int pkcs11_gen_key
(
	ENGINE * e,
	int slot_nr,
	unsigned int key_size,
	unsigned char *key_id,
	size_t key_id_len,
	char *key_label
);
static int pkcs11_del_obj
(
	ENGINE * e,
	int slot_nr,
	char *type_str,
	unsigned char *id,
	size_t id_len,
	char *label
);
static int parse_del_obj_string
(
	const char *del_obj_str,
	int *slot,
	char **type,
	unsigned char *id,
	size_t * id_len,
	char **label
);
static int parse_key_string
(
	const char *pubkey_get_str,
	int *slot,
	unsigned char *id,
	size_t * id_len,
	char **label,
	char **key_file
);

static PKCS11_CTX *ctx = NULL;

/** 
 * The PIN used for login. Cache for the get_pin function.
 * The memory for this PIN is always owned internally,
 * and may be freed as necessary. Before freeing, the PIN 
 * must be whitened, to prevent security holes.
 */
static char *pin = NULL;
static int pin_length = 0;

static int verbose = 0;

static char *module = NULL;

static char *init_args = NULL;

static void Log(int const loglevel, char const * const format, ...)
{
	char tmp[256];
	va_list args;

	va_start(args, format);
	vsnprintf(tmp, 256, format, args);
	va_end(args);

	// log it
	syslog(loglevel, "engine_pkcs11: %s", tmp);
	fprintf(stdout, "%s\n", tmp);
}


int set_module(const char *modulename)
{
	module = modulename ? strdup(modulename) : NULL;
	return 1;
}

/**
 * Set the PIN used for login. A copy of the PIN shall be made.
 *
 * If the PIN cannot be assigned, the value 0 shall be returned
 * and errno shall be set as follows:
 *
 *   EINVAL - a NULL PIN was supplied
 *   ENOMEM - insufficient memory to copy the PIN
 *
 * @param _pin the pin to use for login. Must not be NULL.
 *
 * @return 1 on success, 0 on failure.
 */
int set_pin(const char *_pin)
{
	/* Pre-condition check */
	if (_pin == NULL) {
		errno = EINVAL;
		return 0;
	}

	/* Copy the PIN. If the string cannot be copied, NULL
	   shall be returned and errno shall be set. */
	pin = strdup(_pin);
	if (pin != NULL)
		pin_length = strlen(pin);

	return (pin != NULL);
}

int inc_verbose(void)
{
	verbose++;
	return 1;
}

/* either get the pin code from the supplied callback data, or get the pin
 * via asking our self. In both cases keep a copy of the pin code in the
 * pin variable (strdup'ed copy). */
static int get_pin(UI_METHOD * ui_method, void *callback_data)
{
	UI *ui;
	struct {
		const void *password;
		const char *prompt_info;
	} *mycb = callback_data;

	/* pin in the call back data, copy and use */
	if (mycb != NULL && mycb->password) {
		pin = (char *)calloc(MAX_PIN_LENGTH, sizeof(char));
		if (!pin)
			return 0;
		strncpy(pin,mycb->password,MAX_PIN_LENGTH);
		pin_length = MAX_PIN_LENGTH;
		return 1;
	}

	/* call ui to ask for a pin */
	ui = UI_new();
	if (ui_method != NULL)
		UI_set_method(ui, ui_method);
	if (callback_data != NULL)
		UI_set_app_data(ui, callback_data);

	if (!UI_add_input_string
	    (ui, "PKCS#11 token PIN: ", 0, pin, 1, MAX_PIN_LENGTH)) {
		Log(LOG_ERR, "UI_add_input_string failed");
		UI_free(ui);
		return 0;
	}
	if (UI_process(ui)) {
		Log(LOG_ERR,  "UI_process failed");
		UI_free(ui);
		return 0;
	}
	UI_free(ui);
	return 1;
}

int set_init_args(const char *init_args_orig)
{
	init_args = init_args_orig ? strdup(init_args_orig) : NULL;
	return 1;
}

static void free_pkcs11_ctx()
{
	if (ctx) {
		PKCS11_CTX_unload(ctx);
		PKCS11_CTX_free(ctx);
		ctx = NULL;
	}
}

int pkcs11_finish(ENGINE * engine)
{
	if (verbose) {
		Log(LOG_INFO,  "engine finish");
	}

	free_pkcs11_ctx();

	if (pin != NULL) {
		OPENSSL_cleanse(pin, pin_length);
		free(pin);
		pin = NULL;
		pin_length = 0;
	}
	return 1;
}

int pkcs11_init(ENGINE * engine)
{
	if (verbose) {
		Log(LOG_INFO,  "initializing engine");
	}
	ctx = PKCS11_CTX_new();
        PKCS11_CTX_init_args(ctx, init_args);
	if (PKCS11_CTX_load(ctx, module) < 0) {
		Log(LOG_ERR,  "unable to load module %s", module);
		return 0;
	}
	return 1;
}

int pkcs11_rsa_finish(RSA * rsa)
{
	if (pin) {
		OPENSSL_cleanse(pin, pin_length);
		free(pin);
		pin = NULL;
		pin_length = 0;
	}
	if (module) {
		free(module);
		module = NULL;
	}
	/* need to free RSA_ex_data? */
	return 1;
}

static int hex_to_bin(const char *in, unsigned char *out, size_t * outlen)
{
	size_t left, count = 0;

	if (in == NULL || *in == '\0') {
		*outlen = 0;
		return 1;
	}

	left = *outlen;

	while (*in != '\0') {
		int byte = 0, nybbles = 2;

		while (nybbles-- && *in && *in != ':') {
			char c;
			byte <<= 4;
			c = *in++;
			if ('0' <= c && c <= '9')
				c -= '0';
			else if ('a' <= c && c <= 'f')
				c = c - 'a' + 10;
			else if ('A' <= c && c <= 'F')
				c = c - 'A' + 10;
			else {
				Log(LOG_ERR,
					"hex_to_bin(): invalid char '%c' in hex string",
					c);
				*outlen = 0;
				return 0;
			}
			byte |= c;
		}
		if (*in == ':')
			in++;
		if (left <= 0) {
			Log(LOG_ERR,  "hex_to_bin(): hex string too long");
			*outlen = 0;
			return 0;
		}
		out[count++] = (unsigned char)byte;
		left--;
	}

	*outlen = count;
	return 1;
}

static int str_to_dec(char const * const inPtr, int *dst) {
	int ret = 1;
	long val;

	char *endptr;

	errno = 0;	/* To distinguish success/failure after call */
	val = strtol(inPtr, &endptr, 10);

	/* Check for various possible errors */
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) || (errno != 0 && val == 0)) {
		Log(LOG_ERR, "String to dec conversion failed");
		ret = 0;
	}
	if (endptr == inPtr) {
		Log(LOG_ERR, "No digits were found");
		ret = 0;
	}
	if(ret) {
		*dst = (int)val;
	}
	return ret;
}

/* parse string containing slot and id information */

static int parse_slot_id_string(const char *slot_id, int *slot,
				unsigned char *id, size_t * id_len,
				char **label)
{
	int n, i;

	if (!slot_id)
		return 0;

	/* support for several formats */
#define HEXDIGITS "01234567890ABCDEFabcdef"
#define DIGITS "0123456789"

	/* first: pure hex number (id, slot is 0) */
	if (strspn(slot_id, HEXDIGITS) == strlen(slot_id)) {
		/* ah, easiest case: only hex. */
		if ((strlen(slot_id) + 1) / 2 > *id_len) {
			Log(LOG_ERR,  "id string too long!");
			return 0;
		}
		*slot = 0;
		return hex_to_bin(slot_id, id, id_len);
	}

	/* second: slot:id. slot is an digital int. */
	if (str_to_dec(slot_id, &n) == 1) {
		i = strspn(slot_id, DIGITS);

		if (slot_id[i] != ':') {
			Log(LOG_ERR,  "could not parse string!");
			return 0;
		}
		i++;
		if (slot_id[i] == 0) {
			*slot = n;
			*id_len = 0;
			return 1;
		}
		if (strspn(slot_id + i, HEXDIGITS) + i != strlen(slot_id)) {
			Log(LOG_ERR,  "could not parse string!");
			return 0;
		}
		/* ah, rest is hex */
		if ((strlen(slot_id) - i + 1) / 2 > *id_len) {
			Log(LOG_ERR,  "id string too long!");
			return 0;
		}
		*slot = n;
		return hex_to_bin(slot_id + i, id, id_len);
	}

	/* third: id_<id>  */
	if (strncmp(slot_id, "id_", 3) == 0) {
		if (strspn(slot_id + 3, HEXDIGITS) + 3 != strlen(slot_id)) {
			Log(LOG_ERR,  "could not parse string!");
			return 0;
		}
		/* ah, rest is hex */
		if ((strlen(slot_id) - 3 + 1) / 2 > *id_len) {
			Log(LOG_ERR,  "id string too long!");
			return 0;
		}
		*slot = 0;
		return hex_to_bin(slot_id + 3, id, id_len);
	}

	/* label_<label>  */
	if (strncmp(slot_id, "label_", 6) == 0) {
		*label = strdup(slot_id + 6);
		return *label != NULL;
	}

	/* last try: it has to be slot_<slot> and then "-id_<cert>" */

	if (strncmp(slot_id, "slot_", 5) != 0) {
		Log(LOG_ERR,  "format not recognized!");
		return 0;
	}

	/* slot is an digital int. */
	if (str_to_dec(slot_id + 5, &n) != 1) {
		Log(LOG_ERR,  "slot number not deciphered!");
		return 0;
	}

	i = strspn(slot_id + 5, DIGITS);

	if (slot_id[i + 5] == 0) {
		*slot = n;
		*id_len = 0;
		return 1; 
	}

	if (slot_id[i + 5] != '-') {
		Log(LOG_ERR,  "could not parse string!");
		return 0;
	}

	i = 5 + i + 1;

	/* now followed by "id_" */
	if (strncmp(slot_id + i, "id_", 3) == 0) {
		if (strspn(slot_id + i + 3, HEXDIGITS) + 3 + i !=
		    strlen(slot_id)) {
			Log(LOG_ERR,  "could not parse string!");
			return 0;
		}
		/* ah, rest is hex */
		if ((strlen(slot_id) - i - 3 + 1) / 2 > *id_len) {
			Log(LOG_ERR,  "id string too long!");
			return 0;
		}
		*slot = n;
		return hex_to_bin(slot_id + i + 3, id, id_len);
	}

	/* ... or "label_" */
	if (strncmp(slot_id + i, "label_", 6) == 0) {
		*slot = n;
		return (*label = strdup(slot_id + i + 6)) != NULL;
	}

	Log(LOG_ERR,  "could not parse string!");
	return 0;
}

#define MAX_VALUE_LEN	200

static X509 *pkcs11_load_cert
(
	ENGINE * e,
	int slot_nr,
	unsigned char *cert_id,
	size_t cert_id_len,
	char *cert_label,
	struct cert_info **cert_info,
	int *cert_cnt,
	int enumerate
)
{
	PKCS11_SLOT *slot_list, *slot;
	PKCS11_SLOT *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_CERT *certs, *selected_cert = NULL;
	X509 *x509 = NULL;
	unsigned int slot_count, cert_count, n, m;
	char flags[FLAGS_LEN];
	int local_init = 0;

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}

	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count) < 0) {
		fail("failed to enumerate slots");
	}
	if (verbose) {
		Log(LOG_INFO,  "Found %u slot%s", slot_count,
			(slot_count <= 1) ? "" : "s");
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", (FLAGS_LEN - strlen(flags) - 1));
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->loginRequired)
				strncat(flags, "login, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->readOnly)
				strncat(flags, "ro, ", (FLAGS_LEN - strlen(flags) - 1));
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO,  "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO,  "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (found_slot) {
		slot = found_slot; 
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", (int)slot_nr);
		goto err_out;
	}

	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_ERR,  "Found empty token; ");
		goto err_out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

	if ((tok->loginRequired) && (do_login(slot))) {
		Log(LOG_ERR,  "failed to login");
		goto err_out;
	}

	if (PKCS11_enumerate_certs(tok, &certs, &cert_count)) {
		Log(LOG_ERR,  "unable to enumerate certificates");
		goto err_out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u cert%s:", cert_count,
			(cert_count <= 1) ? "" : "s");
	}

	if (!cert_count)
		goto out;

	if (enumerate) {
		*cert_info = calloc(cert_count, sizeof(struct cert_info));
		if (!*cert_info)
			goto err_out;

		/* Copy all certificates */
		for (n = 0; n < cert_count; n++) {
			PKCS11_CERT *k = certs + n;
			struct cert_info *c = *cert_info + n;

			if (k->id_len) {
				c->id = malloc(k->id_len);
				if (!c->id)
					goto err_out;
				memcpy(c->id, k->id, k->id_len);
				c->id_len = k->id_len;
			}
			if (strlen(k->label))
				c->label = strdup(k->label);
			c->x509 = X509_dup(k->x509);
		}
		*cert_cnt = n;
		goto out;
	}

	/* Find the matching cert */
	if (cert_id_len || cert_label) {
		for (n = 0; n < cert_count; n++) {
			PKCS11_CERT *k = certs + n;

			if (cert_label == NULL) {
				if (cert_id_len != 0 && k->id_len == cert_id_len &&
			    	memcmp(k->id, cert_id, cert_id_len) == 0) {
					selected_cert = k;
				}
			} else {
				if (strcmp(k->label, cert_label) == 0) {
					selected_cert = k;
				}
			}
		}
	} else {
		selected_cert = certs;	/* use first */
	}

	if (selected_cert == NULL) {
		Log(LOG_ERR,  "certificate not found.");
		goto err_out;
	}

	x509 = X509_dup(selected_cert->x509);
	goto out;

err_out:
	if (*cert_info) {
		for (n = 0; n < cert_count; n++) {
			PKCS11_CERT *k = certs + n;
			struct cert_info *c = *cert_info + n;

			if (!c->id)
				free(c->id);
			if (!c->label)
				free(c->label);
			if (!c->x509)
				X509_free(c->x509);
		}
		free(*cert_info);
		*cert_info = NULL;
	}

	PKCS11_release_all_slots(ctx, slot_list, slot_count);

out:
	if (local_init) {
		free_pkcs11_ctx();
	}
	return x509;
}

int load_cert_ctrl(ENGINE * e, void *p)
{
	struct cert_ctrl_params *params = p;

	if (params->cert != NULL)
		return 0;

	params->cert = pkcs11_load_cert(e, params->slot_nr, params->cert_id,
				params->cert_id_len, params->cert_label,
				NULL, NULL, 0);

	if (params->cert == NULL)
		return 0;

	return 1;

}

int enumerate_certs_ctrl(ENGINE * e, void *p)
{
	struct enum_certs_ctrl_param *params = p;

	if (params->info != NULL)
		return 0;

	pkcs11_load_cert(e, params->slot_nr, 0, 0, NULL,
			&params->info, &params->cnt, 1);

	if (params->info == NULL)
		return 0;

	return 1;

}

static EVP_PKEY *pkcs11_load_key
(
	ENGINE * e,
	int slot_nr,
	unsigned char *key_id,
	size_t key_id_len,
	char *key_label,
	UI_METHOD * ui_method,
	void *callback_data,
	int isPrivate
)
{
	PKCS11_SLOT *slot_list, *slot;
	PKCS11_SLOT *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_KEY *keys, *selected_key = NULL;
	PKCS11_CERT *certs;
	EVP_PKEY *pk = NULL;
	unsigned int slot_count, cert_count, key_count, n, m;
	char flags[FLAGS_LEN];
	int local_init = 0;

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}

	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count) < 0)
		fail("failed to enumerate slots");

	if (verbose) {
		Log(LOG_INFO,  "Found %u slot%s", slot_count,
			(slot_count <= 1) ? "" : "s");
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", FLAGS_LEN - strlen(flags) - 1);
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", FLAGS_LEN - strlen(flags) - 1);
			if (slot->token->loginRequired)
				strncat(flags, "login, ", FLAGS_LEN - strlen(flags) - 1);
			if (slot->token->readOnly)
				strncat(flags, "ro, ", FLAGS_LEN - strlen(flags) - 1);
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO,  "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO,  "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (slot_nr == -1) {
		if (!(slot = PKCS11_find_token(ctx, slot_list, slot_count)))
			fail("didn't find any tokens");
	} else if (found_slot) {
		slot = found_slot;
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", slot_nr);
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}
	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_INFO,  "Found empty token; ");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	if (isPrivate && !tok->userPinSet && !tok->readOnly) {
		Log(LOG_INFO,  "Found slot without user PIN");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

#ifdef DEBUG
	if (PKCS11_enumerate_certs(tok, &certs, &cert_count))
		fail("unable to enumerate certificates");

	if (verbose) {
		Log(LOG_INFO,  "Found %u certificate%s:", cert_count,
			(cert_count <= 1) ? "" : "s");
		for (n = 0; n < cert_count; n++) {
			PKCS11_CERT *c = certs + n;
			char *dn = NULL;

			Log(LOG_INFO,  "  %2u	%s", n + 1, c->label);
			if (c->x509)
				dn = X509_NAME_oneline(X509_get_subject_name
						       (c->x509), NULL, 0);
			if (dn) {
				Log(LOG_INFO,  " (%s)", dn);
				OPENSSL_free(dn);
			}
		}
	}
#endif

	/* Perform login to the token if required */
	if (tok->loginRequired) {
		/* If the token has a secure login (i.e., an external keypad),
		   then use a NULL pin. Otherwise, check if a PIN exists. If
		   not, allocate and obtain a new PIN. */
		if (tok->secureLogin) {
			/* Free the PIN if it has already been 
			   assigned (i.e, cached by get_pin) */
			if (pin != NULL) {
				OPENSSL_cleanse(pin, pin_length);
				free(pin);
				pin = NULL;
				pin_length = 0;
			}
		} else if (pin == NULL) {
			pin = (char *)calloc(MAX_PIN_LENGTH, sizeof(char));
			pin_length = MAX_PIN_LENGTH;
			if (pin == NULL) {
				fail("Could not allocate memory for PIN");
			}
			if (!get_pin(ui_method, callback_data) ) {
				OPENSSL_cleanse(pin, pin_length);
				free(pin);
				pin = NULL;
				pin_length = 0;
				fail("No pin code was entered");
			}
		}

		/* Now login in with the (possibly NULL) pin */
		if (PKCS11_login(slot, 0, pin)) {
			/* Login failed, so free the PIN if present */
			if (pin != NULL) {
				OPENSSL_cleanse(pin, pin_length);
				free(pin);
				pin = NULL;
				pin_length = 0;
			}
			fail("Login failed");
		}
		/* Login successful, PIN retained in case further logins are 
		   required. This will occur on subsequent calls to the
		   pkcs11_load_key function. Subsequent login calls should be
		   relatively fast (the token should maintain its own login
		   state), although there may still be a slight performance 
		   penalty. We could maintain state noting that successful
		   login has been performed, but this state may not be updated
		   if the token is removed and reinserted between calls. It
		   seems safer to retain the PIN and peform a login on each
		   call to pkcs11_load_key, even if this may not be strictly
		   necessary. */
		/* TODO when does PIN get freed after successful login? */
		/* TODO confirm that multiple login attempts do not introduce
		   significant performance penalties */

	}

	/* Make sure there is at least one private key on the token */
	if (PKCS11_enumerate_keys(tok, &keys, &key_count)) {
		fail("unable to enumerate keys");
	}
	if (key_count == 0) {
		fail("No keys found.");
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u key%s:", key_count,
			(key_count <= 1) ? "" : "s");
	}
	if ((key_id_len != 0) || (key_label != NULL)) {
		for (n = 0; n < key_count; n++) {
			PKCS11_KEY *k = keys + n;

			if (verbose) {
				Log(LOG_INFO,  "  %2u %c%c %s", n + 1,
					k->isPrivate ? 'P' : ' ',
					k->needLogin ? 'L' : ' ', k->label);
			}

			if (!!isPrivate != !!(k->isPrivate)) continue;

			if (key_label == NULL) {
				if (key_id_len != 0 && k->id_len == key_id_len
				    && (memcmp(k->id, key_id, key_id_len) == 0)) {
					selected_key = k;
				}
			} else {
				if (strcmp(k->label, key_label) == 0) {
					selected_key = k;
				}
			}
		}
	} else {
		selected_key = keys;	/* use first */
	}

	if (selected_key == NULL) {
		Log(LOG_ERR,  "key not found.");
		goto out;
	}

	if (isPrivate) {
		pk = PKCS11_get_private_key(selected_key);
	} else {
		pk = PKCS11_get_public_key(selected_key);
	}

out:
	if (local_init) {
		free_pkcs11_ctx();
	}

	return pk;
}

EVP_PKEY *pkcs11_load_public_key(ENGINE * e, const char *s_key_id,
				 UI_METHOD * ui_method, void *callback_data)
{
	int n;
	unsigned char key_id[MAX_VALUE_LEN / 2];
	size_t key_id_len = sizeof(key_id);
	char *key_label = NULL;
	int slot_nr = -1;
	EVP_PKEY *pk;

	n = parse_key_string(s_key_id, &slot_nr, key_id, &key_id_len,
				&key_label, NULL);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:id_<id>:label_<label>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual key label string.");
		return NULL;
	}

	pk = pkcs11_load_key(e, slot_nr, key_id, key_id_len, key_label,
			ui_method, callback_data, 0);
	if (pk == NULL)
		Log(LOG_INFO,  "PKCS11_load_public_key returned NULL");

    if (key_label) free(key_label);
	return pk;
}

EVP_PKEY *pkcs11_load_private_key(ENGINE * e, const char *s_key_id,
				  UI_METHOD * ui_method, void *callback_data)
{
	int n;
	unsigned char key_id[MAX_VALUE_LEN / 2];
	size_t key_id_len = sizeof(key_id);
	char *key_label = NULL;
	int slot_nr = -1;
	EVP_PKEY *pk;

	n = parse_key_string(s_key_id, &slot_nr, key_id, &key_id_len,
				&key_label, NULL);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:id_<id>:label_<label>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual key label string.");
		return NULL;
	}

	pk = pkcs11_load_key(e, slot_nr, key_id, key_id_len, key_label,
			ui_method, callback_data, 1);
	if (pk == NULL)
		Log(LOG_INFO,  "PKCS11_get_private_key returned NULL");

    if (key_label) free(key_label);
	return pk;
}

int list_token_objects
(
	ENGINE * e,
	long slot_nr /* slot number */
)
{
	PKCS11_SLOT *slot_list, *slot, *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_CERT *certs;
	PKCS11_KEY *keys;
	unsigned int slot_count, cert_count, key_count, m, n, i;
	char flags[FLAGS_LEN];
	int local_init = 0;
	int ret_val = 0;

	if (verbose) {
		Log(LOG_INFO,  "Displaying token objects from slot %d", (int)slot_nr);
	}

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}
	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count)) {
		Log(LOG_ERR,  "failed to enumerate slots");
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u slots", slot_count);
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", (FLAGS_LEN - strlen(flags) - 1));
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->loginRequired)
				strncat(flags, "login, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->readOnly)
				strncat(flags, "ro, ", (FLAGS_LEN - strlen(flags) - 1));
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO,  "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO,  "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (found_slot) {
		slot = found_slot; 
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", (int)slot_nr);
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_ERR,  "Found empty token; ");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

	if ((tok->loginRequired) && (do_login(slot))) {
		Log(LOG_ERR,  "failed to login");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	if (PKCS11_enumerate_certs(tok, &certs, &cert_count)) {
		Log(LOG_ERR,  "unable to enumerate certificates");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	for (n=0; n<cert_count; n++) {
		char tmp[MAX_VALUE_LEN];
		PKCS11_CERT *c = certs + n;
		char *dn = NULL;

		Log(LOG_INFO,  "Certificate Object:");
		Log(LOG_INFO,  "\ttype: X.509 cert");
		Log(LOG_INFO,  "\tlabel: %s", c->label);
		for (i = 0; i < c->id_len; i++) {
			snprintf(&tmp[i*2], 3, "%02x", c->id[i]);
		}
		Log(LOG_INFO,  "\tID: %s", tmp);

		if (c->x509)
			dn = X509_NAME_oneline(X509_get_subject_name(c->x509),
						NULL, 0);
		if (dn) {
			Log(LOG_INFO,  "\tname: %s", dn);
			OPENSSL_free(dn);
		}
	}

	if (PKCS11_enumerate_keys(tok, &keys, &key_count)) {
		Log(LOG_ERR,  "unable to enumerate keys");
		PKCS11_release_all_slots(ctx, slot_list, slot_count);
		goto out;
	}

	for (n=0; n<key_count; n++) {
		char tmp[MAX_VALUE_LEN];

		PKCS11_KEY *k = keys + n;

		if (k->isPrivate) {
			Log(LOG_INFO,  "Private Key Object:");
			Log(LOG_INFO,  "\ttype: RSA");
		} else {
			Log(LOG_INFO,  "Public Key Object:");
			Log(LOG_INFO,  "\ttype: RSA %d bits",
				(PKCS11_get_key_size(k)*8)); 
		}
		Log(LOG_INFO,  "\tlabel: %s", k->label);
		for (i = 0; i < k->id_len; i++) {
			snprintf(&tmp[i*2], 3, "%02x", k->id[i]);
		}
		Log(LOG_INFO,  "\tID: %s", tmp);
	}

	ret_val = 1;

out:
	if (local_init) {
		free_pkcs11_ctx();
	}

	return ret_val;
}

static int do_login(PKCS11_SLOT *slot)
{
	PKCS11_TOKEN *tok = slot->token;

	if (pin == NULL) {
		Log(LOG_INFO,  "PIN not set");
		return -1;
	}

	/* Now login in with the (possibly NULL) pin */
	if (PKCS11_login(slot, 0, pin)) {
	/* Login failed, so free the PIN if present */
		if (pin != NULL) {
			OPENSSL_cleanse(pin, pin_length);
			free(pin);
			pin = NULL;
			pin_length = 0;
		}
		Log(LOG_ERR,  "Login failed");
		return -1;
	}
	return 0;
}

int store_cert_ctrl(ENGINE * e, void *p)
{
	struct cert_ctrl_params *params = p;

	if (params->cert == NULL)
		return 0;

	if (pkcs11_store_cert(e, params->slot_nr, params->cert_id,
						params->cert_id_len, params->cert_label, params->cert))
		return 0;

	return 1;
}

int store_cert_cmd(ENGINE * e, const char *p)
{
	int n, rv=0;
	unsigned char cert_id[MAX_VALUE_LEN / 2];
	size_t cert_id_len = sizeof(cert_id);
	char *cert_label = NULL;
	char *cert_file = NULL;
	int slot_nr = -1;
	unsigned char data[MAX_OBJECT_SIZE];
	const unsigned char *pp = data;
	int data_len = 0;
	FILE *f = NULL;
	X509 *x;

	if (!p || !*p) {
		Log(LOG_ERR,  "no parameter");
		return 0;
	}

	n = parse_cert_store_string(p, &slot_nr, cert_id, &cert_id_len,
								&cert_label, &cert_file);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:id_<id>:label_<label>:cert_<filename>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual cert label string.");
		Log(LOG_INFO,
			"and <filename> is the cert filename with full path.");
		goto store_cert_err_out;
	}

	f = fopen(cert_file, "rb");
	if (f == NULL) {
		Log(LOG_ERR,  "Couldn't open cert file \"%s\"", cert_file);
		goto store_cert_err_out;
	}

	data_len = fread(data, 1, sizeof(data), f);

	if (data_len < 0) {
		Log(LOG_ERR,  "Couldn't read from file \"%s\"", cert_file);
		goto store_cert_err_out;
	}

	x = d2i_X509(NULL, &pp, data_len); 
	if (!x) {
		Log(LOG_ERR,  "OpenSSL cert parse error");
		goto store_cert_err_out;
	}

	/* No ID specified */
	if (cert_id_len == sizeof(cert_id))
		cert_id_len = 0;

	if (verbose) {
		char tmp[MAX_VALUE_LEN];
		for (n = 0; n < cert_id_len; n++) {
			snprintf(&tmp[n*2], 3, "%02x", cert_id[n]);
		}
		Log(LOG_INFO, "Storing cert(%s) in slot(%d) with label(%s) and id(%s)",
			cert_file, slot_nr, cert_label, tmp);
	}

	if (pkcs11_store_cert(e, slot_nr, cert_id, cert_id_len, cert_label, x))
		goto store_cert_err_out;

	rv = 1;

store_cert_err_out:
	if (f) fclose(f);
    if (cert_label) free(cert_label);
    if (cert_file) free(cert_file);
	return rv;
}

static int pkcs11_store_cert
(
	ENGINE * e,
	int slot_nr,
	unsigned char *cert_id,
	size_t cert_id_len,
	char *cert_label,
	X509 *x
)
{
	PKCS11_SLOT *slot_list, *slot;
	PKCS11_SLOT *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_CERT *certs, *ret_cert = NULL;
	unsigned int slot_count, cert_count;
	char flags[FLAGS_LEN];
	int n,m;
	int ret_val = -1;
	int local_init = 0;

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}

	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count)) {
		Log(LOG_ERR,  "failed to enumerate slots");
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u slots", slot_count);
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", FLAGS_LEN - strlen(flags) - 1);
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", FLAGS_LEN - strlen(flags) - 1);
			if (slot->token->loginRequired)
				strncat(flags, "login, ", FLAGS_LEN - strlen(flags) - 1);
			if (slot->token->readOnly)
				strncat(flags, "ro, ", FLAGS_LEN - strlen(flags) - 1);
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO,  "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO,  "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (found_slot) {
		slot = found_slot; 
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", (int)slot_nr);
		goto err_out;
	}

	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_ERR,  "Found empty token; ");
		goto err_out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

	if ((tok->loginRequired) && (do_login(slot))) {
		Log(LOG_ERR,  "failed to login");
		goto err_out;
	}

	/* Enumerate certs to initialize libp11 cert count */
	if (PKCS11_enumerate_certs(tok, &certs, &cert_count)) {
		Log(LOG_ERR,  "unable to enumerate certificates");
		goto err_out;
	}

	if (PKCS11_store_certificate(tok, x, cert_label, cert_id,
								cert_id_len, &ret_cert)) {
		Log(LOG_ERR,  "failed to store cert");
		goto err_out;
	}

	ret_val = 0;
	goto out;

err_out:
	PKCS11_release_all_slots(ctx, slot_list, slot_count);

out:
	if(local_init) {
		free_pkcs11_ctx();
	}

	return ret_val;
}

static int parse_cert_store_string
(
	const char *cert_store_str,
	int *slot,
	unsigned char *id,
	size_t * id_len,
	char **label,
	char **cert_file
)
{
	char *token;
	const char *sep = ":";
	char *str, *lasts;
    int rv = -1;

	if (!cert_store_str)
		return -1;

	str = strdup(cert_store_str);
	if (str == NULL)
		return -1;

	/* Default values */
	*slot = 0;
	*label = NULL;
	*cert_file = NULL;

	token = strtok_r(str, sep, &lasts);

	while (token) {

		/* slot identifier */
		if (!strncmp(token, "slot_", 5)) {
			/* slot is a decimal number */
			if (str_to_dec(token + 5, slot) != 1) {
				Log(LOG_ERR,  "slot number not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "id_", 3)) {
			/* certificate ID */
			/* id is hexadecimal number */
			if (!hex_to_bin(token + 3, id, id_len)) {
				Log(LOG_ERR,  "id not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "label_", 6)) {
			/* label */
			/*  label is string */
			*label = strdup(token + 6);
			if (*label == NULL) {
				Log(LOG_ERR,  "label not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "cert_", 5)) {
			/* cert file name */
			*cert_file = strdup(token + 5);
			if (*cert_file == NULL) {
				Log(LOG_ERR,  "cert file not deciphered!");
                goto err;
			}
		}
		token = strtok_r(NULL, sep, &lasts);
	} /* end while(token) */

	if (*cert_file == NULL) {
		Log(LOG_ERR,  "cert file name not present!");
		goto err;
	}

    rv = 0;	

err: 
    if (str) free(str);
    if (rv && *label) { free(*label); *label = NULL; }
    if (rv && *cert_file) { free(*cert_file); *cert_file = NULL; }
    return rv;
}

int gen_key_cmd(ENGINE * e, const char *p)
{
	int key_size, n;
	unsigned char key_id[MAX_VALUE_LEN / 2];
	size_t key_id_len = sizeof(key_id);
	char *key_label = NULL;
	int slot_nr = -1;
    int rv = 0;

	if (!p || !*p) {
		Log(LOG_ERR,  "no parameter");
		return 0;
	}

	n = parse_key_gen_string(p, &slot_nr, &key_size, key_id, &key_id_len,
							 &key_label);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:size_<size>:id_<id>:label_<label>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <size> is the RSA key size in bits.");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual cert label string.");
		goto gen_key_err_out;
	}

	if (verbose) {
		char tmp[MAX_VALUE_LEN];
		for (n = 0; n < key_id_len; n++) {
			snprintf(&tmp[n*2], 3, "%02x", key_id[n]);
		}
		Log(LOG_INFO, "Generating %dbits RSA key in slot(%d) with label(%s) and id(s)",
			key_size, slot_nr, key_label, tmp);
	}

	if (pkcs11_gen_key(e, slot_nr, (unsigned int)key_size, key_id, key_id_len, key_label))
		goto gen_key_err_out;

	rv = 1;

gen_key_err_out:
    if (key_label) free(key_label);
	return rv;

}

static int parse_key_gen_string
(
	const char *key_gen_str,
	int *slot,
	int *key_size,
	unsigned char *id,
	size_t * id_len,
	char **label
)
{
	char *token;
	const char *sep = ":";
	char *str, *lasts;
    int rv = -1;

	if (!key_gen_str)
		return -1;

	str = strdup(key_gen_str);
	if (str == NULL)
		return -1;

	/* Default values */
	*slot = 0;
	*label = NULL;
	*key_size = -1;

	token = strtok_r(str, sep, &lasts);

	while (token) {

		/* slot identifier */
		if (!strncmp(token, "slot_", 5)) {
			/* slot is a decimal number */
			if (str_to_dec(token + 5, slot) != 1) {
				Log(LOG_ERR,  "slot number not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "id_", 3)) {
			/* certificate ID */
			/* id is hexadecimal number */
			if (!hex_to_bin(token + 3, id, id_len)) {
				Log(LOG_ERR,  "id not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "label_", 6)) {
			/* label */
			/*  label is string */
			*label = strdup(token + 6);
			if (*label == NULL) {
				Log(LOG_ERR,  "label not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "size_", 5)) {
			/* key size in bits */
			if (str_to_dec(token + 5, key_size) != 1) {
				Log(LOG_ERR,  "key size not deciphered!");
                goto err;
			}
		}
		token = strtok_r(NULL, sep, &lasts);
	} /* end while(token) */

	if (*key_size == -1) {
		Log(LOG_ERR,  "key size not present!");
        goto err;
	}

	rv = 0;
err:
    if (str) free(str);
    if (rv && *label) { free(*label); *label = NULL; }
    return rv;
}

static int pkcs11_gen_key
(
	ENGINE * e,
	int slot_nr,
	unsigned int key_size,
	unsigned char *key_id,
	size_t key_id_len,
	char *key_label
)
{
	PKCS11_SLOT *slot_list, *slot;
	PKCS11_SLOT *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_KEY *keys, *ret_key = NULL;
	unsigned int slot_count, key_count;
	char flags[FLAGS_LEN];
	int n,m;
	int ret_val = -1;
	int local_init = 0;

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}

	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count)) {
		Log(LOG_ERR,  "failed to enumerate slots");
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u slots", slot_count);
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", (FLAGS_LEN - strlen(flags) - 1));
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->loginRequired)
				strncat(flags, "login, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->readOnly)
				strncat(flags, "ro, ", (FLAGS_LEN - strlen(flags) - 1));
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO,  "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO,  "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (found_slot) {
		slot = found_slot; 
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", (int)slot_nr);
		goto err_out;
	}

	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_ERR,  "Found empty token; ");
		goto err_out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

	if ((tok->loginRequired) && (do_login(slot))) {
		Log(LOG_ERR,  "failed to login");
		goto err_out;
	}

	/* Enumerate keys to initialize libp11 key count */
	if (PKCS11_enumerate_keys(tok, &keys, &key_count)) {
		Log(LOG_ERR,  "unable to enumerate keys");
		goto err_out;
	}

	if (PKCS11_generate_key(tok, EVP_PKEY_RSA, key_size, key_label,
							key_id, key_id_len)) {
		Log(LOG_ERR,  "failed to generate RSA key pair");
		goto err_out;
	}

	ret_val = 0;
	goto out;

err_out:
	PKCS11_release_all_slots(ctx, slot_list, slot_count);
out:
	if (local_init) {
		free_pkcs11_ctx();
	}
	return ret_val;
}

int del_obj_cmd(ENGINE * e, const char *p)
{
	int n;
	unsigned char id[MAX_VALUE_LEN / 2];
	size_t id_len = sizeof(id);
	char *label = NULL, *type = NULL;
	int slot_nr = -1;
    int rv = 0;

	if (!p || !*p) {
		Log(LOG_ERR,  "no parameter");
		return 0;
	}

	n = parse_del_obj_string(p, &slot_nr, &type, id, &id_len, &label);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:type_<type>:id_<id>:label_<label>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <type> is the object type (privkey/pubkey/cert)");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual label string.");
		goto err;
	}

	/* No ID specified */
	if (id_len == sizeof(id)) {
		id_len = 0;
	}

	if (verbose) {
		char tmp[MAX_VALUE_LEN];
		for (n = 0; n < id_len; n++) {
			snprintf(&tmp[n*2], 3, "%02x", id[n]);
		}
		Log(LOG_INFO, "Deleting object type(%s) from slot(%d) with label(%s) and id(%s)",
			type, slot_nr, label, tmp);
	}

	if (pkcs11_del_obj(e, slot_nr, type, id, id_len, label))
		goto err;

	rv = 1;

err:
    if (label) free(label);
    if (type) free(type);
    return rv;
}

static int parse_del_obj_string
(
	const char *del_obj_str,
	int *slot,
	char **type,
	unsigned char *id,
	size_t * id_len,
	char **label
)
{
	char *token;
	const char *sep = ":";
	char *str, *lasts;
    int rv = -1;

	if (!del_obj_str)
		return -1;

	str = strdup(del_obj_str);
	if (str == NULL)
		return -1;

	/* Default values */
	*slot = 0;
	*label = NULL;
	*type = NULL;

	token = strtok_r(str, sep, &lasts);

	while (token) {

		/* slot identifier */
		if (!strncmp(token, "slot_", 5)) {
			/* slot is a decimal number */
			if (str_to_dec(token + 5, slot) != 1) {
				Log(LOG_ERR,  "slot number not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "id_", 3)) {
			/* certificate ID */
			/* id is hexadecimal number */
			if (!hex_to_bin(token + 3, id, id_len)) {
				Log(LOG_ERR,  "id not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "label_", 6)) {
			/* label */
			/*  label is string */
			*label = strdup(token + 6);
			if (*label == NULL) {
				Log(LOG_ERR,  "label not deciphered!");
                goto err;
			}
		} else if (!strncmp(token, "type_", 5)) {
			/* Object type */
			*type = strdup(token + 5);
			if (*type == NULL) {
				Log(LOG_ERR,  "type not deciphered!");
                goto err;
			}
		}
		token = strtok_r(NULL, sep, &lasts);
	} /* end while(token) */

	if (*type == NULL) {
		Log(LOG_ERR,  "type not present!");
        goto err;
	}

	rv = 0;
err:
    if (str) free(str);
    if (rv && *label) { free(*label); *label = NULL; }
    if (rv && *type) { free(*type); *type = NULL; }
    return rv;
}

static int pkcs11_del_obj
(
	ENGINE * e,
	int slot_nr,
	char *type_str,
	unsigned char *id,
	size_t id_len,
	char *label
)
{
	PKCS11_SLOT *slot_list, *slot;
	PKCS11_SLOT *found_slot = NULL;
	PKCS11_TOKEN *tok;
	PKCS11_KEY *keys, *ret_key = NULL;
	unsigned int slot_count, key_count;
	char flags[FLAGS_LEN];
	int n,m, type, retval = -1;
	int local_init = 0;
#define TYPE_PRIVKEY	1
#define TYPE_PUBKEY	2
#define TYPE_CERT	3

	if (!strcmp(type_str, "privkey")) {
		type = TYPE_PRIVKEY;
	} else if (!strcmp(type_str, "pubkey")) {
		type = TYPE_PUBKEY;
	} else if (!strcmp(type_str, "cert")) {
		type = TYPE_CERT;
	} else {
		Log(LOG_ERR,  "invalid object type(%s)", type_str);
		goto out;
	}

	if (ctx == NULL) {
		pkcs11_init(e);
		local_init = 1;
	}

	if (PKCS11_enumerate_slots(ctx, &slot_list, &slot_count)) {
		Log(LOG_ERR,  "failed to enumerate slots");
		goto out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found %u slots", slot_count);
	}

	for (n = 0; n < slot_count; n++) {
		slot = slot_list + n;
		flags[0] = '\0';
		if (slot->token) {
			if (!slot->token->initialized)
				strncat(flags, "uninitialized, ", (FLAGS_LEN - strlen(flags) - 1));
			else if (!slot->token->userPinSet)
				strncat(flags, "no pin, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->loginRequired)
				strncat(flags, "login, ", (FLAGS_LEN - strlen(flags) - 1));
			if (slot->token->readOnly)
				strncat(flags, "ro, ", (FLAGS_LEN - strlen(flags) - 1));
		} else {
			strncpy(flags, "no token", sizeof(flags));
		}
		if ((m = strlen(flags)) != 0) {
			flags[m - 2] = '\0';
		}

		if (slot_nr != -1 &&
			slot_nr == PKCS11_get_slotid_from_slot(slot)) {
			found_slot = slot;
		}

		if (verbose) {
			Log(LOG_INFO, "[%lu] %-25.25s  %-16s",
				PKCS11_get_slotid_from_slot(slot),
				slot->description, flags);
			if (slot->token) {
				Log(LOG_INFO, "  (%s)",
					slot->token->label[0] ?
					slot->token->label : "no label");
			}
		}
	}

	if (found_slot) {
		slot = found_slot; 
	} else {
		Log(LOG_ERR,  "Invalid slot number: %d", (int)slot_nr);
		goto err_out;
	}

	tok = slot->token;

	if (tok == NULL) {
		Log(LOG_ERR,  "Found empty token; ");
		goto err_out;
	}

	if (verbose) {
		Log(LOG_INFO,  "Found slot:  %s", slot->description);
		Log(LOG_INFO,  "Found token: %s", slot->token->label);
	}

	if ((tok->loginRequired) && (do_login(slot))) {
		Log(LOG_ERR,  "failed to login");
		goto err_out;
	}

	switch(type) {
		case TYPE_CERT:
			if (PKCS11_remove_certificate(tok, label, id, id_len)) {
				Log(LOG_ERR,  "failed to delete certificate");
				goto err_out;
			}
			break;

		case TYPE_PUBKEY:
			if (PKCS11_remove_public_key(tok, label, id, id_len)) {
				Log(LOG_ERR,  "failed to delete public key");
				goto err_out;
			}
			break;

		case TYPE_PRIVKEY:
			if (PKCS11_remove_private_key(tok, label, id, id_len)) {
				Log(LOG_ERR,  "failed to delete private key");
				goto err_out;
			}
			break;

		default:
			Log(LOG_ERR,  "object type(%s) not supported", type_str);
			goto err_out;
			break;
	}

	retval = 0;
err_out:
	PKCS11_release_all_slots(ctx, slot_list, slot_count);
out:
	if (local_init) {
		free_pkcs11_ctx();
	}
	return retval;
}

int get_pubkey_cmd(ENGINE * e, const char *p)
{
	int n, rv = 0;
	unsigned char key_id[MAX_VALUE_LEN / 2];
	size_t key_id_len = sizeof(key_id);
	char *key_label = NULL;
	char *key_file = NULL;
	int slot_nr = -1;
	unsigned char *pp, *data = NULL;
	int data_len = 0;
	FILE *f = NULL;
	EVP_PKEY *pk;
	RSA *rsa;

	if (!p || !*p) {
		Log(LOG_ERR,  "no parameter");
		return 0;
	}

	n = parse_key_string(p, &slot_nr, key_id, &key_id_len,
				&key_label, &key_file);
	if (n) {
		Log(LOG_INFO,
			"supported format: slot_<slot>:id_<id>:label_<label>:key_<filename>");
		Log(LOG_INFO,
			"where <slot> is the slot number as normal integer,");
		Log(LOG_INFO,
			"and <id> is the id number as hex string.");
		Log(LOG_INFO,
			"and <label> is the textual key label string.");
		Log(LOG_INFO,
			"and <filename> is the key filename with full path.");
		goto get_pubkey_err_out;
	}

	f = fopen(key_file, "wb");
	if (f == NULL) {
		Log(LOG_ERR,  "Couldn't open key file \"%s\"", key_file);
		goto get_pubkey_err_out;
	}

	/* No ID specified */
	if (key_id_len == sizeof(key_id))
		key_id_len = 0;

	if (verbose) {
		char tmp[MAX_VALUE_LEN];
		for (n = 0; n < key_id_len; n++) {
			snprintf(&tmp[n*2], 3, "%02x", key_id[n]);
		}
		Log(LOG_INFO, "Getting public key in slot(%d) with label(%s) and id(%s)",
			slot_nr, key_label, tmp);
	}

	pk = pkcs11_load_key(e, slot_nr, key_id, key_id_len, key_label, NULL, NULL, 0);
	if (pk == NULL) {
		Log(LOG_ERR,  "PKCS11_load_public_key returned NULL");
		goto get_pubkey_err_out;
	}

	rsa = EVP_PKEY_get1_RSA(pk);
	if (rsa == NULL) {
		Log(LOG_ERR,  "Couldn't retrieve RSA key form EVP_PKEY");
		goto get_pubkey_err_out;
	}

#if 0
	/* To store in DER format */
	data_len = i2d_RSAPublicKey(rsa, NULL); 
	if ((data = (unsigned char *)malloc(data_len)) == NULL) {
		Log(LOG_ERR,  "couldn't allocate memory for key");
		goto get_pubkey_err_out;
	}
	pp = data;
	data_len = i2d_RSAPublicKey(rsa, &pp); 
	n = fwrite(data, 1, data_len, f);

	if (n != data_len) {
		Log(LOG_ERR,  "Couldn't write to file \"%s\"", key_file);
		goto get_pubkey_err_out;
	}
#endif
	if (!PEM_write_RSA_PUBKEY(f, rsa)) {
		Log(LOG_ERR,  "Couldn't write to file \"%s\"", key_file);
		goto get_pubkey_err_out;
	}

	rv = 1;

get_pubkey_err_out:
	if (f) fclose(f);
	if (key_label) free(key_label);
	if (key_file) free(key_file);
	return rv;

}

static int parse_key_string
(
	const char *key_get_str,
	int *slot,
	unsigned char *id,
	size_t * id_len,
	char **label,
	char **key_file
)
{
	char *token;
	const char *sep = ":";
	char *str, *lasts;
    int rv = -1;

	if (!key_get_str) {
		return -1;
	}
	str = strdup(key_get_str);
	if (str == NULL) {
		return -1;
	}
	/* Default values */
	*slot = 0;
	*label = NULL;
	if (key_file) {
		*key_file = NULL;
	}
	token = strtok_r(str, sep, &lasts);

	while (token) {

		/* slot identifier */
		if (!strncmp(token, "slot_", 5)) {
			/* slot is a decimal number */
			if (str_to_dec(token + 5, slot) != 1) {
				Log(LOG_ERR,  "slot number not deciphered!");
				goto err;
			}
		} else if (!strncmp(token, "id_", 3)) {
			/* certificate ID */
			/* id is hexadecimal number */
			if (!hex_to_bin(token + 3, id, id_len)) {
				Log(LOG_ERR,  "id not deciphered!");
				goto err;
			}
		} else if (!strncmp(token, "label_", 6)) {
			/* label */
			/*  label is string */
			*label = strdup(token + 6);
			if (*label == NULL) {
				Log(LOG_ERR,  "label not deciphered!");
				goto err;
			}
		} else if ((key_file) && (!strncmp(token, "key_", 4))) {
			/* key file name */
			*key_file = strdup(token + 4);
			if (*key_file == NULL) {
				Log(LOG_ERR,  "key file not deciphered!");
				goto err;
			}
		}
		token = strtok_r(NULL, sep, &lasts);
	} /* end while(token) */

	if ((key_file) && (*key_file == NULL)) {
		Log(LOG_ERR,  "key file name not present!");
		goto err;
	}

	rv = 0;

err:
	if (str) free(str);
    if (rv && *label) { free(*label); *label = NULL; }
    if (rv && *key_file) { free(*key_file); *key_file = NULL; }
	return rv;
}

