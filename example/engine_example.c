/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* Example application demonstrating the use of the engine via OpenSSL API's */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <openssl/x509.h>
#include <openssl/engine.h>
#include <openssl/err.h>

#define MAX_OBJECT_SZ   2048
#define ENGINE_SOPATH "/usr//lib/engines/engine_pkcs11.so"
#define ENGINE_MODULEPATH "/usr/lib/softhsm/libsecstore.so.1"
#define PKCS11_ENGINE_PIN "1234"

#define PKCS11_ENGINE_ID "pkcs11"
#define PKCS11_ENGINE_NAME "pkcs11 engine"

#define CMD_SO_PATH		ENGINE_CMD_BASE
#define CMD_MODULE_PATH 	(ENGINE_CMD_BASE+1)
#define CMD_PIN		(ENGINE_CMD_BASE+2)
#define CMD_VERBOSE		(ENGINE_CMD_BASE+3)
#define CMD_QUIET		(ENGINE_CMD_BASE+4)
#define CMD_LOAD_CERT_CTRL	(ENGINE_CMD_BASE+5)
#define CMD_INIT_ARGS	(ENGINE_CMD_BASE+6)
#define CMD_LIST_OBJS	(ENGINE_CMD_BASE+7)
#define CMD_STORE_CERT	(ENGINE_CMD_BASE+8)
#define CMD_STORE_CERT_CTRL	(ENGINE_CMD_BASE+9)
#define CMD_GEN_KEY	(ENGINE_CMD_BASE+10)
#define CMD_DEL_OBJ	(ENGINE_CMD_BASE+11)
#define CMD_GET_PUBKEY	(ENGINE_CMD_BASE+12)
#define CMD_ENUMERATE_CERTS_CTRL	(ENGINE_CMD_BASE+13)

struct cert_info {
	unsigned char *id;
	size_t id_len;
	char *label;
	X509 *x509;
};

struct enum_certs_ctrl_param {
	int slot_nr;
	int cnt;
	struct cert_info *info;
};

struct cert_ctrl_params {
    int slot_nr;
    unsigned char *cert_id;
    size_t cert_id_len;
    char *cert_label;
    X509 *cert;
};

#define CLOCK_TYPE1 CLOCK_MONOTONIC
#define CLOCK_TYPE2 CLOCK_PROCESS_CPUTIME_ID
#define CLOCK_TYPE CLOCK_TYPE1

ENGINE *setup_engine()
{
    ENGINE *e = NULL;
    const char *engine_id;

    ENGINE_load_dynamic();

    e = ENGINE_by_id("dynamic");

    if (!ENGINE_ctrl_cmd_string(e, "SO_PATH", ENGINE_SOPATH, 0)) {
        printf("ERR: SO_PATH failed\n");
        return NULL;
    }

    if (!ENGINE_ctrl_cmd_string(e, "ID", PKCS11_ENGINE_ID, 0)) {
        printf("ERR: ID failed\n");
        return NULL;
    }

    if (!ENGINE_ctrl_cmd_string(e, "LIST_ADD", "1", 0)) {
        printf("ERR: LIST_ADD failed\n");
        return NULL;
    }

    if (!ENGINE_ctrl_cmd_string(e, "LOAD", NULL, 0)) {
        printf("ERR: LOAD failed\n");
        return NULL;
    }

    if (!ENGINE_ctrl_cmd_string(e, "MODULE_PATH", ENGINE_MODULEPATH, 0)) {
        printf("ERR: MODULE_PATH failed\n");
        return NULL;
    }

    engine_id = ENGINE_get_id(e);

    //printf("engine id: %s\n", engine_id);

    e = ENGINE_by_id(engine_id);

    if (e == NULL) {
        printf("ERR: Couldn't get ENGINE_by_id\n");
        return NULL;
    }

#if 0
    /* For Debug */
    if (!ENGINE_ctrl_cmd_string(e, "VERBOSE", NULL, 0)) {
        printf("ERR: VERBOSE failed\n");
        goto err;
    }
#endif

    if (!ENGINE_ctrl_cmd_string(e, "PIN", PKCS11_ENGINE_PIN, 0)) {
        printf("ERR: PIN failed\n");
        goto err;
    }

    if (!ENGINE_init(e)) {
        printf("ERR: ENGINE_init(e) failed, reason:\n");
        goto err;
    }

    return e;
err:
    ENGINE_finish(e);
    return e;
}

int store_cert
(
    ENGINE *e,
    char *file,
    int slot_nr,
    unsigned char *id,
    size_t id_len,
    char *label
)
{
    struct cert_ctrl_params params;

    FILE *fp;
    int len, rv = -1;
    unsigned char data[MAX_OBJECT_SZ];
    const unsigned char *pp = data;
    ENGINE_CTRL_FUNC_PTR ctrl_func;

    fp = fopen(file, "rb");
    if (fp == NULL) {
        printf("ERR: Couldn't open cert file(%s)\n", file);
        goto err;
    }

    len = fread(data, 1, sizeof(data), fp);
    if (len < 0) {
        printf("ERR: Couldn't read from cert file(%s)\n", file);
        goto err;
    }

    params.cert = d2i_X509(NULL, &pp, len);
    if (!params.cert) {
        printf("ERR: OpenSSL cert parse error\n");
        goto err;
    }

    params.slot_nr = slot_nr;
    params.cert_id = id;
    params.cert_id_len = id_len;
    params.cert_label = label;

    ctrl_func = ENGINE_get_ctrl_function(e);
    if (ctrl_func == NULL) {
        printf("ERR: ENGINE_get_ctrl_func failed\n");
        goto err;
    }

    if (!ctrl_func(e, CMD_STORE_CERT_CTRL, 0, &params, NULL)) {
        printf("ERR: CMD_STORE_CERT_CTRL failed\n");
        goto err;
    } else {
        printf("CMD_STORE_CERT_CTRL successful\n");
    }

    rv = 0;
err:
    if (fp)
        fclose(fp);
    return rv;
}

int load_cert
(
    ENGINE *e,
    char *file,
    int slot_nr,
    unsigned char *id,
    size_t id_len,
    char *label
)
{
    struct cert_ctrl_params params;

    FILE *fp;
    int len, rv = -1;
    ENGINE_CTRL_FUNC_PTR ctrl_func;

    struct timespec t0, t1;

    fp = fopen(file, "wb");
    if (fp == NULL) {
        printf("ERR: Couldn't open cert file(%s)\n", file);
        goto err;
    }

    memset(&params, 0, sizeof(params));
    params.slot_nr = slot_nr;
    params.cert_id = id;
    params.cert_id_len = id_len;
    params.cert_label = label;

    ctrl_func = ENGINE_get_ctrl_function(e);
    if (ctrl_func == NULL) {
        printf("ERR: ENGINE_get_ctrl_func failed\n");
        goto err;
    }

    if (clock_gettime(CLOCK_TYPE, &t0)) {
        printf("%s: Failed to get t0\n", __func__);
    }
    rv = ctrl_func(e, CMD_LOAD_CERT_CTRL, 0, &params, NULL);
    if (clock_gettime(CLOCK_TYPE, &t1)) {
        printf("%s: Failed to get t1\n", __func__);
    }
    if (!rv) {
        printf("ERR: CMD_LOAD_CERT_CTRL failed\n");
	rv = -1;
        goto err;
    } else {
        printf("CMD_LOAD_CERT_CTRL successful\n");
    }

    printf("%s: t0(%ld, %ldns), t1(%ld, %ldns), d(%ld, %ldns)\n", __func__,
		    t0.tv_sec, t0.tv_nsec, t1.tv_sec, t1.tv_nsec,
		    (t1.tv_sec - t0.tv_sec), (t1.tv_nsec - t0.tv_nsec));

    if (!i2d_X509_fp(fp, params.cert)) {
        printf("ERR: i2d_X509_fp failed\n");
        goto err;
    }

    rv = 0;
err:
    if (fp) fclose(fp);
    if (params.cert) X509_free(params.cert);
    return rv;
}

int enumerate_certs
(
    ENGINE *e,
    int slot_nr
)
{

    FILE *fp;
    int len, i, j, rv = -1;
    ENGINE_CTRL_FUNC_PTR ctrl_func;
    struct enum_certs_ctrl_param params;

    struct timespec t0, t1;

    memset(&params, 0, sizeof(params));
    params.slot_nr = slot_nr;

    ctrl_func = ENGINE_get_ctrl_function(e);
    if (ctrl_func == NULL) {
        printf("ERR: ENGINE_get_ctrl_func failed\n");
        goto err;
    }

    if (clock_gettime(CLOCK_TYPE, &t0)) {
        printf("%s: Failed to get t0\n", __func__);
    }
    rv = ctrl_func(e, CMD_ENUMERATE_CERTS_CTRL, 0, &params, NULL);
    if (clock_gettime(CLOCK_TYPE, &t1)) {
        printf("%s: Failed to get t1\n", __func__);
    }
    if (!rv) {
        printf("ERR: CMD_ENUMERATE_CERTS_CTRL failed\n");
	rv = -1;
        goto err;
    } else {
        printf("CMD_ENUMERATE_CERTS_CTRL successful\n");
    }

    printf("%s: t0(%ld, %ldns), t1(%ld, %ldns), d(%ld, %ldns)\n", __func__,
		    t0.tv_sec, t0.tv_nsec, t1.tv_sec, t1.tv_nsec,
		    (t1.tv_sec - t0.tv_sec), (t1.tv_nsec - t0.tv_nsec));

    /* List all the certificates */
    for (i = 0; i < params.cnt; i++) {
	struct cert_info *c = params.info + i;
	printf("%s: label: %s\n", __func__, c->label);
	printf("%s: ID: "); 
	for (j = 0; j < c->id_len; j++)
		printf("%02x", c->id[j]);
	printf("\n");
	if (c->label)
		free(c->label);
	if (c->id_len)
		free(c->id);
	if (c->x509)
		X509_free(c->x509);
    }

    if (params.info)
	    free(params.info);
    rv = 0;

err:
    return rv;
}

int sign_verify
(
    ENGINE *e
)
{
    int rv = -1;
    unsigned int siglen;
    EVP_PKEY *pub_key = NULL, *priv_key = NULL;
    EVP_MD_CTX *ctx = NULL;
    const EVP_MD *md;
    char *key_id = "slot_0:id_04:label_tkey";
    unsigned char data[512], *sig = NULL;

    struct timespec t0, t1;

    pub_key = ENGINE_load_public_key(e, key_id, NULL, NULL);
    if (pub_key == NULL) {
        printf("ERR: load_public_key failed\n");
        goto err;
    }

    priv_key = ENGINE_load_private_key(e, key_id, NULL, NULL);
    if (priv_key == NULL) {
        printf("ERR: load_private_key failed\n");
        goto err;
    }

    if (!ENGINE_set_default_RSA(e)) {
        printf("ERR: set_default_RSA failed\n");
        goto err;
    }

    md = EVP_sha1();
    if (md == NULL) {
        printf("ERR: EVP_get_digestbyname failed\n");
        goto err;
    }

    ctx = EVP_MD_CTX_create();
    if (!ctx) {
        printf("ERR: EVP_MD_CTX_create failed\n");
        goto err;
    }

    siglen = EVP_PKEY_size(priv_key);
    if (!siglen) {
        printf("ERR: EVP_PKEY_size failed\n");
        goto err;
    }

    sig = malloc(siglen);
    if (sig == NULL) {
        printf("ERR: sig malloc failed\n");
        goto err;
    }

    /* Sign operation */
    EVP_MD_CTX_init(ctx);

    if (clock_gettime(CLOCK_TYPE, &t0)) {
        printf("%s: Failed to get t0\n", __func__);
    }

    if (!EVP_SignInit(ctx, md)) {
        printf("ERR: EVP_SignInit failed\n");
        goto err;
    }

    if (!EVP_SignUpdate(ctx, data, sizeof(data))) {
        printf("ERR: EVP_SignUpdate failed\n");
        goto err;
    }

    rv = EVP_SignFinal(ctx, sig, &siglen, priv_key);

    if (clock_gettime(CLOCK_TYPE, &t1)) {
        printf("%s: Failed to get t1\n", __func__);
    }

    printf("%s: sign: t0(%ld, %ldns), t1(%ld, %ldns), d(%ld, %ldns)\n",
		    __func__, t0.tv_sec, t0.tv_nsec, t1.tv_sec, t1.tv_nsec,
		    (t1.tv_sec - t0.tv_sec), (t1.tv_nsec - t0.tv_nsec));

    if (!rv) {
        printf("ERR: EVP_SignFinal failed (%s)\n",
			ERR_error_string(ERR_get_error(), NULL));
	rv = -1;
        goto err;
    } else {
        printf("EVP_SignFinal successful\n");
    }

    /* Verify operation */
    EVP_MD_CTX_init(ctx);

    if (clock_gettime(CLOCK_TYPE, &t0)) {
        printf("%s: Failed to get t0\n", __func__);
    }

    if (!EVP_VerifyInit(ctx, md)) {
        printf("ERR: EVP_VerifyInit failed\n");
        goto err;
    }

    if (!EVP_VerifyUpdate(ctx, data, sizeof(data))) {
        printf("ERR: EVP_VerifyUpdate failed\n");
        goto err;
    }

    rv = EVP_VerifyFinal(ctx, sig, siglen, pub_key);

    if (clock_gettime(CLOCK_TYPE, &t1)) {
        printf("%s: Failed to get t1\n", __func__);
    }

    printf("%s: Verify: t0(%ld, %ldns), t1(%ld, %ldns), d(%ld, %ldns)\n",
		    __func__, t0.tv_sec, t0.tv_nsec, t1.tv_sec, t1.tv_nsec,
		    (t1.tv_sec - t0.tv_sec), (t1.tv_nsec - t0.tv_nsec));

    if (!rv) {
        printf("ERR: EVP_VerifyFinal failed (%s)\n",
			ERR_error_string(ERR_get_error(), NULL));
	rv = -1;
        goto err;
    } else {
        printf("EVP_VerifyFinal successful\n");
    }

    rv = 0;

err:
    if (ctx) EVP_MD_CTX_destroy(ctx);
    if (priv_key) EVP_PKEY_free(priv_key);
    if (pub_key) EVP_PKEY_free(pub_key);
    if (sig) free(sig);
    return rv;
}

int main()
{
    ENGINE *e = NULL;
    char *file = "caCert.der";
    char *lfile = "lcaCert.der";
    int slot_nr = 0;
    unsigned char id = 0x06;
    size_t id_len = sizeof(id);
    char *cert_label = "tcert";

    printf("\n");
    printf("NOTE: Before running the example ensure the following:\n");
    printf("\tAn X.509 DER encoded certificate named caCert.der\n\tshould be present in the current directory\n");
    printf("\tAn RSA key pair with ID(04) and label(tkey) is present\n\tin the secure store\n");
    printf("\n");

    e = setup_engine();
    if (e == NULL) {
        printf("ERR: setup_engine failed\n");
        return -1;
    }

    if (store_cert(e, file, slot_nr, &id, id_len, cert_label)) {
        printf("ERR: store_cert failed\n");
    }

    if (load_cert(e, lfile, slot_nr, &id, id_len, cert_label)) {
        printf("ERR: load_cert failed\n");
    }

    if (enumerate_certs(e, slot_nr)) {
        printf("ERR: enumerate_certs failed\n");
    }

    if (sign_verify(e)) {
        printf("ERR: sign_verify failed\n");
    }

    ENGINE_finish(e);
    return 0;
}

